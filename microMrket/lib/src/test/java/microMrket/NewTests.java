package microMrket;
import org.junit.Test;

import jdk.jfr.Timestamp;

import static org.junit.Assert.*;

import java.util.ArrayList;


public class NewTests {
    @Test
    public void garlicTest() {
        Product product = new Product("Garlic Butter", "Dairy",15 );
        double resultWithDiscount = product.CalculateDiscount(product,10 );
        double result=product.result;
        double expected = 150;
        assertEquals(expected, resultWithDiscount,150);
        assertEquals(expected, result,150);
    }
    @Test
    public void newTypeProduct(){
        Product product=new Product("Noodles", "Pasta", 5.50);
        double resultWithDiscount = product.CalculateDiscount(product, 20);
        double result = product.result;
        assertEquals(55,resultWithDiscount,55);
        assertEquals(110, result,110);
    }
    @Test
    public void thirdTest() {
        OrderDetail order =new OrderDetail();
        Product product = new Product("Garlic Butter", "Dairy",15 );
        order.CalculateInvoice(10, product);
        Invoice invoice1 = new Invoice(product.getName(), product.getPrice(), 7, product.result,
                product.resultWithDiscount);
        
        Product product2=new Product("Noodles", "Pasta", 5.50);
        order.CalculateInvoice(20, product2);
        Invoice invoice2 = new Invoice(product2.getName(), product2.getPrice(), 7, product.result,
                product2.resultWithDiscount);
        ArrayList<Invoice> invoice = new ArrayList<>();
        invoice.add(invoice1);
        invoice.add(invoice2);
        double total = order.calculateAllCost(invoice);

        assertEquals(150, product.result,150);
        assertEquals(150, product.resultWithDiscount,150);
        assertEquals(55, product2.result,55);
        assertEquals(110, product2.resultWithDiscount,110);
        assertEquals(205,total,205);
    }
}

