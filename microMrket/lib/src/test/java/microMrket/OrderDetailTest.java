package microMrket;
import org.junit.Test;
import static org.junit.Assert.*;

import java.util.ArrayList;

public class OrderDetailTest {
    @Test
    public void CalculateInvoice() {

        OrderDetail order = new OrderDetail();
        Product product = new Product("milk", "Dairy", 5);
        order.CalculateInvoice(7, product);
        Invoice invoice1 = new Invoice(product.getName(), product.getPrice(), 7, product.result,
                product.resultWithDiscount);
        Product product2 = new Product("milk", "Dairy", 5);
        order.CalculateInvoice(7, product2);
        Invoice invoice2 = new Invoice(product.getName(), product.getPrice(), 7, product.result,
                product.resultWithDiscount);

        ArrayList<Invoice> invoice = new ArrayList<>();
        invoice.add(invoice1);
        invoice.add(invoice2);
        ArrayList<Invoice> expecteds = new ArrayList<>();
        expecteds = new ArrayList<>();

        assertNotSame(expecteds, invoice);
    }

    @Test
    public void totalAllTest() {
        OrderDetail order = new OrderDetail();
        Product product = new Product("milk", "Dairy", 5);
        order.CalculateInvoice(7, product);
        Invoice invoice1 = new Invoice(product.getName(), product.getPrice(), 7, product.result,
                product.resultWithDiscount);
        Product product2 = new Product("Tomato", "Vegetable", 2);
        order.CalculateInvoice(10, product2);
        Invoice invoice2 = new Invoice(product.getName(), product.getPrice(), 7, product.result,
                product.resultWithDiscount);

        ArrayList<Invoice> invoice = new ArrayList<>();
        invoice.add(invoice1);
        invoice.add(invoice2);

        double total = order.calculateAllCost(invoice);

        assertEquals(55.0, total, 55);

    }
}

