package microMrket;
import java.util.ArrayList;

public class OrderDetail {
    double totalPrice;
    double totalAll;
    double totalPriceWithDiscount;
    ArrayList<Invoice> invoice = new ArrayList<Invoice>();

    public ArrayList<Invoice> CalculateInvoice(int quantity, Product product) {
        ArrayList<Invoice> bill = new ArrayList<>();
        product.CalculateDiscount(product, quantity);
        totalPrice = product.result;
        totalPriceWithDiscount = product.resultWithDiscount;
        return bill;

    }

    public double calculateAllCost(ArrayList<Invoice> invoice) {
        for (int index = 0; index < invoice.size(); index++) {
            totalAll = invoice.get(index).getTotalDiscount() + totalAll;
        }
        return totalAll;

    }

}
