package microMrket;
import org.junit.Test;
import static org.junit.Assert.*;

public class ProductTest {
    @Test
    public void oneProductCreationtest() {
        Product product1 = new Product("milk", "Dairy", 5);
        assertEquals("milk", product1.getName());
        assertEquals("Dairy", product1.getType());
    }

    @Test

    public void multipleProductCreationTest() {
        Product product1 = new Product("milk", "Dairy", 5);
        Product product2 = new Product("Tomato", "Vegetable", 1);
        Product product3 = new Product("Gelato", "IceCream", 7);
        Product product4 = new Product("Bread", null, 0.50);

       
        assertEquals("milk", product1.getName());
        assertEquals("Tomato", product2.getName());
        assertEquals("Gelato", product3.getName());
        assertEquals("Bread", product4.getName());
    }
}

