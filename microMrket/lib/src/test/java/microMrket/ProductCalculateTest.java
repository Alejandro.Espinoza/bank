package microMrket;
import org.junit.Test;
import static org.junit.Assert.*;




public class ProductCalculateTest {
    @Test
    public void CalculateDiscountTest() {
        Product product = new Product("Onion","Vegetable",2);
        double result =product.CalculateDiscount(product, 35);
        
        double  expected = 65.5;
        assertEquals(expected ,result,65.6);


    }
    @Test
    public void CalculateDiscountTest2() {
        Product product = new Product("bread",null,0.50);
        double result=product.CalculateDiscount(product, 20);
        double  expected = 10;
        assertEquals(expected ,result,10);


    }
    
}
