package microMrket;
import org.junit.Test;
import static org.junit.Assert.*;

public class InvoiceTests {
    @Test
    public void addInvoice() {
        Invoice invoice = new Invoice("milk",5, 15, 75, 67.5);
        assertEquals("milk", invoice.getName());
        assertEquals(75, invoice.getTotal(),0.0);
        assertEquals(67.5, invoice.getTotalDiscount(),0.0);
    }
}

