package microMrket;
public class Invoice {

    private String name;
    private double quantity;
    private double price;
    private double total;
    private double totalDiscount;

    public Invoice(String Name, double Price, int Quantity, double Total, double TotalDiscount) {
        name = Name;
        price = Price;
        total = Total;
        totalDiscount = TotalDiscount;
        quantity = Quantity;

    }

    public String getName() {
        return name;
    }

    public double getTotal() {
        return total;
    }

    public double getPrice() {
        return price;
    }

    public double getTotalDiscount() {
        return totalDiscount;
    }

    public double getQuantity() {
        return quantity;
    }

}
