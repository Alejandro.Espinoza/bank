package microMrket;
package prime.factors;



public class Product {
    

    private double price;
    private int dairyQuantity = 10;
    private int candyQuantity = 20;
    private int vegetablesQuantity = 30;
    private String productName;
    private String productType;
    private double pastaDiscount =50;
    private double vegetablesDiscount = 5;
    private double candyDiscount = 5;
    private double dairyDiscount = 10;

    double result;
    double resultWithDiscount;

    public Product(String name, String type, double Price) {
        productName = name;
        productType = type;
        price = Price;
    }

    public String getName() {
        return productName;
    }

    public String getType() {
        return productType;
    }

    public double getPrice() {
        return price;
    }

    public double CalculateDiscount(Product product, int quantity) {
        
        if (product.getType() == "Dairy" && quantity > dairyQuantity) {
            result = product.getPrice() * quantity;
            resultWithDiscount= result - (result* dairyDiscount/100);

        }
        if (product.getType() == "Pasta" && quantity > pastaDiscount) {
            result = product.getPrice() * quantity;
            resultWithDiscount= result - (result* pastaDiscount/100);

        }

        if (product.getType() == "Candy" && quantity > candyQuantity) {
            result = product.getPrice() * quantity;
            resultWithDiscount= result - (result* candyDiscount/100);
        }

        if (product.getType() == "Vegetable" && quantity > vegetablesQuantity) {
            result = product.getPrice() * quantity;
            resultWithDiscount= result - (result* vegetablesDiscount/100);

        } else {
            result = product.getPrice() * quantity;
            resultWithDiscount = result;
        }


        return resultWithDiscount;

    }
}